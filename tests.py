from configs import Global
from locators import Index
from selenium.webdriver.common.by import By
import xmlrunner

__author__ = 'fomars'
import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

class WordFinderTests(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox()

    def test_check_title(self):
        driver = self.driver
        driver.get(Global.site_url)
        self.assertIn("Word Finder", driver.title)

    def test_input(self):
        driver = self.driver
        driver.get(Global.site_url)
        input = driver.find_element(*Index.INPUT_PROMPT)
        input.send_keys('n_')
        input.submit()
        self.assertIn('Words found', driver.find_element(*Index.LIST_HEADER).text)

    def test_hello(self):
        self.driver.get(Global.site_url)
        input = self.driver.find_element(*Index.INPUT_PROMPT)
        input.send_keys('he__o')
        input.submit()
        self.assertIn('HELLO', self.driver.page_source)

    def test_link(self):
        self.driver.get(Global.site_url)
        input = self.driver.find_element(*Index.INPUT_PROMPT)
        input.send_keys('he__o')
        input.submit()
        self.driver.get(self.driver.find_element_by_link_text('HELLO').get_attribute('href'))
        self.assertIn("define hello", self.driver.title.lower())

    def test_that_fails(self):
        self.driver.get(Global.site_url)
        self.assertIn('Hello world', self.driver.title)

    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main(
        testRunner=xmlrunner.XMLTestRunner(output='reports'),
        # these make sure that some options that are not applicable
        # remain hidden from the help menu.
        failfast=False, buffer=False, catchbreak=False, exit=False)